<?php 
class LogDb extends CDbLogRoute
{
    protected function processLogs($logs)
    {
        $command=$this->getDbConnection()->createCommand();
        $logTime=date('Y-m-d H:i:s'); //Get Current Date
 
        if (Yii::app()->user->getId()) {
            foreach($logs as $log)
            {
                $command->insert($this->logTableName,array(
                    'level'=>$log[1],
                    'category'=>$log[2],
                    'logtime'=>$logTime,
                    'ip_user'=> Yii::app()->request->userHostAddress, //Get Ip
                    'usuario_id'=>Yii::app()->user->getId() , //Get name
                    'request_url'=>Yii::app()->request->url, // Get Url
                    'message'=>$log[0]
                ));
            }
        }
    }
 
}
?>