<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property integer $id
 * @property integer $perfil_id
 * @property string $nome
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Marca[] $marcas
 * @property Carro[] $carros
 * @property Perfil $perfil
 * @property Log[] $logs
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('perfil_id, nome, email, username, password', 'required'),
			array('perfil_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>60),
			array('email', 'length', 'max'=>40),
			array('username', 'length', 'max'=>30),
			array('password', 'length', 'max'=>130),
			array('created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, perfil_id, nome, email, username, password, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'marcas' => array(self::HAS_MANY, 'Marca', 'usuario_id'),
			'carros' => array(self::HAS_MANY, 'Carro', 'usuario_id'),
			'perfil' => array(self::BELONGS_TO, 'Perfil', 'perfil_id'),
			'logs' => array(self::HAS_MANY, 'Log', 'usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'perfil_id' => 'Perfil',
			'nome' => 'Nome',
			'email' => 'Email',
			'username' => 'Username',
			'password' => 'Password',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('perfil_id',$this->perfil_id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
