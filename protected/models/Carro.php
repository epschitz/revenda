<?php

/**
 * This is the model class for table "carro".
 *
 * The followings are the available columns in table 'carro':
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $marca_id
 * @property string $modelo
 * @property integer $ano
 * @property string $foto
 * @property string $valor
 * @property string $valor_juros
 * @property integer $numero_parcelas
 * @property string $created_at
 *
 * The followings are the available model relations:
 * @property Usuario $usuario
 * @property Marca $marca
 */
class Carro extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'carro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario_id, marca_id, modelo, ano, valor, valor_juros, numero_parcelas', 'required'),
			array('usuario_id, marca_id, ano, numero_parcelas', 'numerical', 'integerOnly'=>true),
			array('modelo, foto', 'length', 'max'=>40),
			array('valor, valor_juros', 'length', 'max'=>14),
			array('foto', 'file', 'types'=>'jpg, gif, png'),
			array('created_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, usuario_id, marca_id, modelo, ano, foto, valor, valor_juros, numero_parcelas, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
			'marca' => array(self::BELONGS_TO, 'Marca', 'marca_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usuario_id' => 'Usuario',
			'marca_id' => 'Marca',
			'modelo' => 'Modelo',
			'ano' => 'Ano',
			'foto' => 'Foto',
			'valor' => 'Valor',
			'valor_juros' => 'Valor Juros',
			'numero_parcelas' => 'Numero Parcelas',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('marca_id',$this->marca_id);
		$criteria->compare('modelo',$this->modelo,true);
		$criteria->compare('ano',$this->ano);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('valor_juros',$this->valor_juros,true);
		$criteria->compare('numero_parcelas',$this->numero_parcelas);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Carro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
