CREATE TABLE perfil (
    id          SERIAL,
    descricao   VARCHAR(40) NOT NULL,
    CONSTRAINT pk_perfil PRIMARY KEY (id)
);

INSERT INTO perfil (descricao) VALUES ('admin');
INSERT INTO perfil (descricao) VALUES ('funcionario');

CREATE TABLE usuario (
    id          SERIAL,
    perfil_id   INTEGER NOT NULL,
    nome        VARCHAR(60) NOT NULL,
    email       VARCHAR(40) NOT NULL,
    username    VARCHAR(30) NOT NULL,
    password    VARCHAR(130) NOT NULL,
    created_at  TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT pk_usuario PRIMARY KEY (id),
    CONSTRAINT fk_usuario_perfil FOREIGN KEY (perfil_id) REFERENCES perfil(id)
);

INSERT INTO usuario (perfil_id, nome, email, username, password) 
     VALUES (1, 'Eduardo', 'epschitz@gmail.com', 'schitz', 'b9e268c5b6da9ab8b507ce84b5475655');

INSERT INTO usuario (perfil_id, nome, email, username, password) 
     VALUES (2, 'Paculski', 'paculski@teste.com', 'edu', '379ef4bd50c30e261ccfb18dfc626d9f');

CREATE TABLE marca (
    id          SERIAL,
    usuario_id  INTEGER NOT NULL,
    descricao   VARCHAR(40) NOT NULL,
    created_at  TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT pk_marca PRIMARY KEY (id),
    CONSTRAINT fk_marca_usuario FOREIGN KEY (usuario_id) REFERENCES usuario(id)
);

CREATE TABLE carro (
    id                  SERIAL,
    usuario_id          INTEGER NOT NULL,
    marca_id            INTEGER NOT NULL,
    modelo              VARCHAR(40) NOT NULL,
    ano                 INTEGER NOT NULL,
    foto                VARCHAR(40) NULL,
    valor               NUMERIC(14,2) NOT NULL,
    valor_juros         NUMERIC(14,2) NOT NULL,
    numero_parcelas     INTEGER NOT NULL,
    created_at          TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT pk_carro PRIMARY KEY (id),
    CONSTRAINT fk_carro_usuario FOREIGN KEY (usuario_id) REFERENCES usuario(id),
    CONSTRAINT fk_carro_marca   FOREIGN KEY (marca_id)   REFERENCES marca(id)
);

CREATE TABLE log (
    id          SERIAL,
    usuario_id  INTEGER NOT NULL,
    level       VARCHAR(128),
    category    VARCHAR(128),
    logtime     TIMESTAMP DEFAULT current_timestamp,
    ip_user     VARCHAR(50),
    request_url TEXT,
    message     TEXT,
    created_at  TIMESTAMP DEFAULT current_timestamp,
    CONSTRAINT pk_log PRIMARY KEY (id),
    CONSTRAINT fk_log_usuario FOREIGN KEY (usuario_id) REFERENCES usuario(id)
);