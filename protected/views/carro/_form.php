<?php
/* @var $this CarroController */
/* @var $model Carro */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'carro-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'marca_id'); ?>
		<?php
			$perfil_model = Marca::model()->findAll(array('order' => 'descricao')); 
      $list = CHtml::listData($perfil_model, 'id', 'descricao');

			echo $form->dropDownList($model, 'marca_id', $list, array('prompt'=>'Selecione'));
		?>
		<?php echo $form->error($model,'marca_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modelo'); ?>
		<?php echo $form->textField($model,'modelo',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'modelo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ano'); ?>
		<?php echo $form->textField($model,'ano'); ?>
		<?php echo $form->error($model,'ano'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'foto'); ?>
		<?php echo $form->fileField($model,'foto',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'foto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor'); ?>
		<?php 
			$options = array('size'=>14,'maxlength'=>14);
			if (!Yii::app()->controller->isAdmin())
				$options['disabled'] = 'disabled';

			// echo $form->textField($model,'valor',$options);
			$this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'valor',
				'mask' => '9.999,99',
				// 'htmlOptions' => array('size' => 6)
				));

		?>
		<?php echo $form->error($model,'valor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'valor_juros'); ?>
		<?php echo $form->textField($model,'valor_juros',array('size'=>14,'maxlength'=>14)); ?>
		<?php echo $form->error($model,'valor_juros'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'numero_parcelas'); ?>
		<?php echo $form->textField($model,'numero_parcelas'); ?>
		<?php echo $form->error($model,'numero_parcelas'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->